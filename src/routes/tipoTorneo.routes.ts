import { Router } from "express";
import {getTipoTorneo,getTiposTorneos,createTipoTorneo} from "../controllers/tipoTorneo.controller"
const router = Router();

router.get("/",getTiposTorneos);
router.get("/:id",getTipoTorneo);
router.post("/",createTipoTorneo);
/*
router.put("/",editLenguaje);
*/

export = router;
