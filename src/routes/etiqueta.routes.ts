import { Router } from "express";
import {getEtiquetas} from "../controllers/etiqueta.controler"
const router = Router();

router.get("/", getEtiquetas);

export = router