import { Router } from "express";
import {getProblema,getProblemas, crearProblema,editProblema} from "../controllers/problema.controller";
import { isAdmin } from '../middleware/auth'

const router = Router();

router.get("/", getProblemas);
router.get("/:id",getProblema);
router.post("/",[isAdmin],crearProblema);
router.put("/:id",[isAdmin],editProblema);

export = router;
