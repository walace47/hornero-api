import { Router } from "express";
import { getStubs,getStub,descargarStub,createStub,editStub } from "../controllers/stubs.controller";
import { auth,isAdmin } from "../middleware/auth";

const router = Router();

router.get("/", [auth],getStubs);
router.post("/", [isAdmin],createStub);
router.get('/descargar',descargarStub);
router.put("/:id", [isAdmin],editStub);
router.get("/:id", getStub);

export = router;
