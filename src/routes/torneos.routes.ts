import { Router } from "express";
import { getTorneo,getTorneos, obtenerUsuarioPorProblemaResuelto,createTorneo,editTorneo, registrarTorneo} from "../controllers/torneos.controller"
import {auth, isDocente} from "../middleware/auth";

const router = Router();

router.get("/",getTorneos);
router.get("/problemas-resueltos/:id",obtenerUsuarioPorProblemaResuelto)
router.get("/:id",getTorneo);
router.post("/",[isDocente],createTorneo);
router.put("/:id",[isDocente],editTorneo);
router.post("/inscribir/:id",[auth],registrarTorneo)

export = router;
