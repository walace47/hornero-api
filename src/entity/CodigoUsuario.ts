import {Column,Entity,PrimaryGeneratedColumn, ManyToOne, JoinColumn} from "typeorm";
import { Usuario } from "./Usuario";

@Entity("CodigoUsuario" ,{schema:"hornero" } )
export class CodigoUsuario {
    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idCodigoUsuario"
        })
    idCodigoUsuario:number;

 
    @Column("text",{ 
        nullable:false,
        name:"codigo"
        })
    codigo:string;

    @Column("text",{
        nullable:false,
        name:"nombre"
    })
    nombre:string;

    @ManyToOne(()=>Usuario, (user: Usuario)=>user.codigosGuardados,
        {   nullable:false,
            onDelete: 'RESTRICT',
            onUpdate: 'RESTRICT' 
        })
    @JoinColumn({ name:'idUsuario'})
    usuario:Usuario;


}