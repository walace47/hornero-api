import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Problema} from "./Problema";


@Entity("complejidad" ,{schema:"hornero" } )
export class Complejidad {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idComplejidad"
        })
    idComplejidad:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"Complejidad"
        })
    complejidad:string;
   
    @OneToMany(()=>Problema, (problema: Problema)=>problema.complejidad,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    problemas:Problema[];
    
}
