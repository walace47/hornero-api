import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Usuario} from "./Usuario";


@Entity("cambioclave" ,{schema:"hornero" } )
@Index("idUsuario",["usuario",])
export class CambioClave {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idCambio"
        })
    idCambio:number;
        

   
    @ManyToOne(()=>Usuario, (usuario: Usuario)=>usuario.cambioClaves,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idUsuario'})
    usuario:Usuario | null;


    @Column("timestamp",{ 
        nullable:false,
        default: () => "CURRENT_TIMESTAMP",
        name:"fecha"
        })
    fecha:Date;
        

    @Column("varchar",{ 
        nullable:false,
        length:32,
        name:"token"
        })
    token:string;
        

    @Column("int",{ 
        nullable:false,
        name:"estado"
        })
    estado:number;
        
}
