import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Usuario} from "./Usuario";
import {Problema} from "./Problema";
import {Solucion} from "./Solucion";
import {Torneo} from "./Torneo";
import {ESTADO_RESOLUCION, EstadoResolucion} from "./EstadoResolucion";


@Entity("resolucion" ,{schema:"hornero" } )
@Index("Token",["token",],{unique:true})
@Index("idUsuario",["usuario","solucion","torneo",])
@Index("idSolucion",["solucion",])
@Index("idTorneo",["torneo",])
@Index("idEstado",["estado",])
@Index("idProblema",["problema",])
export class Resolucion {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idResolucion"
        })
    idResolucion:number;
        

   
    @ManyToOne(()=>Usuario, (usuario: Usuario)=>usuario.resoluciones,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idUsuario'})
    usuario:Usuario | null;


   
    @ManyToOne(()=>Problema, (problema: Problema)=>problema.resoluciones,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idProblema'})
    problema:Problema | null;


   
    @ManyToOne(()=>Solucion, (solucion: Solucion)=>solucion.resoluciones,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idSolucion'})
    solucion:Solucion | null;


   
    @ManyToOne(()=>Torneo, (torneo: Torneo)=>torneo.resoluciones,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idTorneo'})
    torneo:Torneo | null;


    @Column("varchar",{ 
        nullable:false,
        unique: true,
        length:32,
        name:"Token"
        })
    token:string;
        

    @Column("bigint",{ 
        nullable:false,
        name:"FechaSolicitud"
        })
    fechaSolicitud:string;
        

    @Column("bigint",{ 
        nullable:true,
        name:"FechaRespuesta"
        })
    fechaRespuesta:string | null;
        

    @Column("varchar",{ 
        nullable:true,
        length:2000,
        name:"Respuesta"
        })
    respuesta:string | null;
        

   
    @ManyToOne(()=>EstadoResolucion, (estadoresolucion: EstadoResolucion)=>estadoresolucion.resoluciones,{ onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idEstado'})
    estado:EstadoResolucion | null;

}
