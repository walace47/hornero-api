import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Problema} from "./Problema";
import {Resolucion} from "./Resolucion";


@Entity("solucion" ,{schema:"hornero" } )
@Index("idProblema",["problema"])
export class Solucion {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idSolucion"
        })
    idSolucion:number;
        

   
    @ManyToOne(()=>Problema, (problema: Problema)=>problema.soluciones,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idProblema'})
    problema:Problema | null;


    @Column("varchar",{ 
        nullable:false,
        length:2000,
        name:"ParametrosEntrada"
        })
    parametrosEntrada:string;
        

    @Column("varchar",{ 
        nullable:false,
        length:2000,
        name:"Salida"
        })
    salida:string;
        

   
    @OneToMany(()=>Resolucion, (resolucion: Resolucion)=>resolucion.solucion,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    resoluciones:Resolucion[];
    
}
