import {BaseEntity,Column,Entity,Index,JoinColumn,PrimaryGeneratedColumn, ManyToOne} from "typeorm";
import {Lenguaje} from "./Lenguaje";


@Entity("stub" ,{schema:"hornero" } )
@Index("idLenguaje",["lenguaje",])
export class Stub {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idStubs"
        })
    idStubs:number;
        

   
    @ManyToOne(()=>Lenguaje, (lenguaje: Lenguaje)=>lenguaje.stubs,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idLenguaje'})
    lenguaje:Lenguaje | null;


    @Column("text",{ 
        nullable:false,
        name:"Descripcion"
        })
    descripcion:string;
        

    @Column("varchar",{ 
        nullable:false,
        name:"Archivo"
        })
    archivo:string;
        
}
