import {Column,Entity,PrimaryGeneratedColumn} from "typeorm";

@Entity("etiqueta" ,{schema:"hornero" } )
export class Etiqueta {
    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idEtiqueta"
        })
    idEtiqueta:number;

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"etiqueta"
        })
    etiqueta:string;


}