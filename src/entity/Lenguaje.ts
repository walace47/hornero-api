import {Column,Entity,OneToMany,PrimaryGeneratedColumn} from "typeorm";
import {Stub} from "./Stub";
import {Usuario} from "./Usuario";


@Entity("lenguaje" ,{schema:"hornero" } )
export class Lenguaje {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idLenguaje"
        })
    idLenguaje:number;
       
    @Column("varchar",{ 
        nullable:false,
        name:"Lenguaje"
        })
    lenguaje?:string;

    @OneToMany(()=>Stub, (stub: Stub)=>stub.lenguaje,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    stubs?:Stub[];

    @OneToMany(()=>Usuario, (usuario: Usuario)=>usuario.lenguajeFavorito,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    usuarios?:Usuario[];
    
}
