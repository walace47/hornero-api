import {Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,PrimaryGeneratedColumn} from "typeorm";
import {Complejidad} from "./Complejidad";
import {Resolucion} from "./Resolucion";
import {Solucion} from "./Solucion";
import {TorneoProblema} from "./TorneoProblema";
import { Etiqueta } from "./Etiqueta";

export const problemaRelacion = {
    COMPLEJIDAD: "complejidad",
    RESOLUCIONES:"resoluciones",
    SOLUCIONES: "soluciones",
    ETIQUETAS:"etiquetas"

}

@Entity("problema" ,{schema:"hornero" } )
@Index("idComplejidad",["complejidad",])
export class Problema {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idProblema"
        })
    idProblema:number;

    @Column("varchar",{ 
        nullable:false,
        name:"Nombre"
        })
    nombre:string;
        

    @Column("varchar",{ 
        nullable:true,
        name:"Archivo"
        })
    archivo:string | null;
        

    @Column("text",{ 
        nullable:false,
        name:"Enunciado"
        })
    enunciado:string;
        

   
    @ManyToOne(()=>Complejidad, (complejidad: Complejidad)=>complejidad.problemas,{  nullable:false,onDelete: 'RESTRICT',onUpdate: 'RESTRICT' })
    @JoinColumn({ name:'idComplejidad'})
    complejidad:Complejidad | null;


    @Column("double",{ 
        nullable:false,
        precision:22,
        name:"TiempoEjecucionMax"
        })
    tiempoEjecucionMax:number;
        

   
    @OneToMany(()=>Resolucion, (resolucion: Resolucion)=>resolucion.problema,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    resoluciones:Resolucion[];
    

   
    @OneToMany(()=>Solucion, (solucion: Solucion)=>solucion.problema,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    soluciones:Solucion[];
    

   
    @OneToMany(()=>TorneoProblema, (torneoproblema: TorneoProblema)=>torneoproblema.problema,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    torneoproblemas:TorneoProblema[];
    
    @ManyToMany(type => Etiqueta)
    @JoinTable({
            name:"problemaEtiqueta", 
            joinColumn:{
                name:"idProblema",
                referencedColumnName:"idProblema"
            },
            inverseJoinColumn: {
                name: 'idEtiqueta',
                referencedColumnName: 'idEtiqueta'
            }
    })
    etiquetas: Etiqueta[];
}

