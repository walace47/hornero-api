import {BaseEntity,Column,Entity,Index,JoinColumn,JoinTable,ManyToMany,ManyToOne,OneToMany,OneToOne,PrimaryColumn,PrimaryGeneratedColumn,RelationId} from "typeorm";
import {Usuario} from "./Usuario";


@Entity("rol" ,{schema:"hornero" } )
export class Rol {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idRol"
        })
    idRol:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"Rol"
        })
    rol?:string;
        

   
    @OneToMany(()=>Usuario, (usuario: Usuario)=>usuario.rol,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    usuarios?:Usuario[];
    
}


export enum ROL{
    admin = 1,
    jugador = 2,
    docente = 3
  }
  
