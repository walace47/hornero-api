import {Column,Entity,OneToMany,PrimaryGeneratedColumn} from "typeorm";
import {Torneo} from "./Torneo";

export enum ESTADO_TORNEO {
    AntesDeComienzo = 1,
    EnProceso = 2,
    Terminado = 3
}

@Entity("estadotorneo" ,{schema:"hornero" } )
export class EstadoTorneo {

    @PrimaryGeneratedColumn({
        type:"int", 
        name:"idEstado"
        })
    idEstado:number;
        

    @Column("varchar",{ 
        nullable:false,
        length:100,
        name:"Estado"
        })
    estado:string;
        

   
    @OneToMany(()=>Torneo, (torneo: Torneo)=>torneo.estado,{ onDelete: 'RESTRICT' ,onUpdate: 'RESTRICT' })
    torneos:Torneo[];
    
}
