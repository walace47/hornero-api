const jwt = require('jsonwebtoken');
import { Request, Response, NextFunction } from "express";
import { ROL } from "../entity/Rol";



export const auth = (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = req.get("token");
        req['usuario'] = jwt.verify(token, process.env.KEY).usuario;

        const usuario: object = {
            idUsuario: req['usuario'].idUsuario,
            institucion: req['usuario'].institucion,
            nombreUsuario: req['usuario'].nombreUsuario,
            descripcion: req['usuario'].descripcion,
            email: req['usuario'].email,
            rol: req['usuario'].rol
        };
        const newToken = jwt.sign({ usuario }, process.env.KEY, { expiresIn: process.env.EXPIRACION_TOKEN })
        res.setHeader("Access-Control-Expose-Headers", "*");
        res.setHeader("token", newToken);
        next();
    } catch (err) {
        return res.status(401).json({
            error: err,
            ok: false
        })
    }
}

export const refreshToken = (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = req.get("token");
        req['usuario'] = jwt.verify(token, process.env.KEY).usuario;

        const usuario: object = {
            idUsuario: req['usuario'].idUsuario,
            institucion: req['usuario'].institucion,
            nombreUsuario: req['usuario'].nombreUsuario,
            descripcion: req['usuario'].descripcion,
            email: req['usuario'].email,
            rol: req['usuario'].rol
        };
        const newToken = jwt.sign({ usuario }, process.env.KEY, { expiresIn: process.env.EXPIRACION_TOKEN })
        res.setHeader("Access-Control-Expose-Headers", "*");
        res.setHeader("token", newToken);
        next();
    } catch (err) {
        next();
    }
}

export const isAdmin = (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = req.get("token");
        const payload = jwt.verify(token, process.env.KEY);;
        req['usuario'] = payload.usuario;
        if (payload.usuario.rol.idRol !== ROL.admin) {
            throw { msg: "Necesita ser administrador para acceder a estas funciones" }
        } else {
            next();
        }

    } catch (err) {
        return res.status(401).json({
            error: err,
            ok: false
        })
    }
}


export const isDocente = (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = req.get("token");
        const usuario = jwt.verify(token, process.env.KEY);
        req['usuario'] = usuario.usuario;
        if (usuario.usuario.rol.idRol !== ROL.docente && usuario.usuario.rol.idRol !== ROL.admin) {
            throw { msg: "Necesita ser docente para acceder a estas funciones" }
        } else {
            next();
        }

    } catch (err) {
        return res.status(401).json({
            error: err,
            ok: false
        })
    }
}

export const esSuUsuario = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const token = req.get("token");
        const idUsuario = req.params.id;
        if (!idUsuario) {
            throw new Error("Envie un id en los parametros");
        }
        const payload = jwt.verify(token, process.env.KEY);
        req['usuario'] = payload.usuario;
        if (payload.usuario.rol.idRol !== idUsuario) {
            throw { msg: "No tiene permisos para modificar este usuario" }
        }
        if (payload.usuario.rol.idRol !== ROL.admin) {
            throw { msg: "No tiene permisos para modificar este usuario" }
        } else {
            next();
        }

    } catch (err) {
        return res.status(401).json({
            error: err,
            ok: false
        })
    }
}