import "reflect-metadata";
let cors = require("cors");
import { createConnection } from "typeorm";
import Server from "./server/Server";
import bodyParser = require("body-parser");
import * as routes from './routes/routes.index';
import { ejecutarCron } from './functions/cronJobs'
import { mainHandler } from "./socketController/mainHandler";
import { install } from 'source-map-support';
import { refreshToken } from "./middleware/auth";

createConnection()
	.then(con => {
		install();
		require('dotenv').config();

		const server = Server.init(Number(process.env.PORT));

		//Cron del servidor para actualizar cosas 
		ejecutarCron();
		//Cors
		server.app.use(cors());
		server.app.use(refreshToken)

		//parseo de del cuerpo de los mensaje a objetos de Javascript en res.body   
		server.app.use(bodyParser.json({ limit: "50mb" }));
		server.app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));

		//Rutas Api
		server.app.use('/api', routes);

		//Manejador Sockets
		//blocklyHandler(server.socketIo);
		mainHandler(server.socketIo);

		server.start(() => {
			console.log(`servidor corriendo en el puerto http://localhost:${process.env.PORT}/api`);
		});
	})
	.catch(error =>
		console.log({ error, mg: "No se pudo conectar a la base de datos" })
	);
