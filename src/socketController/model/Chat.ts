import { Mensaje } from "./interfaces";

export class Chat {
    public mensajes:Mensaje[];
  
    constructor(){
      this.mensajes = [];
    }
    agregarMensaje(mensaje:Mensaje){
      if(this.mensajes.length > 20){
        this.mensajes.pop()
      }
      this.mensajes.unshift(mensaje);
    }
  }