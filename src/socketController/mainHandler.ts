import { Server, Socket } from "socket.io";
import {blocklyHandler} from "./blocklyHandler"

let salas = {};

export const mainHandler = (io: Server) => {
    io.on("connection",(socket:Socket)=>{
        blocklyHandler(socket,io,salas);
        
    })
}