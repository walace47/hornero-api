import {  Request, Response } from "express";
import { getRepository } from "typeorm";
import { Etiqueta } from "../entity/Etiqueta";

export const getEtiquetas = async (req: Request, res: Response) => {
    try {
        let result = await getRepository(Etiqueta).find() ;
        res.json(result);
    } catch (error) {
      res.status(500).json({ error });
    }
};