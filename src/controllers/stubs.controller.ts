import { Request, Response } from "express";
import { Stub } from "../entity/Stub";
import { getRepository } from "typeorm";
import { Lenguaje } from "../entity/Lenguaje";


export const getStubs = async (req: Request, res: Response) => {
    try {
        let select = req.query.select ? JSON.parse((req.query.select) as string) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
        let result = await getRepository(Stub).find({ select: select, relations: relations });
        res.json(result);
    } catch (error) {
        res.status(500).json({ error });
    }
}

export const getStub = async (req: Request, res: Response) => {
    try {
        let id = req.params.id;
        let select = req.query.select ? JSON.parse((req.query.select) as string) : ""
        let relations = req.query.relations ? JSON.parse((req.query.relations) as string) : ""
        if (Number(id)) {
            let result = await getRepository(Stub).findOne({ select: select, relations: relations, where: [{ idStubs: id }] });
            res.json(result)
        } else {
            res.status(400).json({ msg: "Id invalido" })
        }
    } catch (error) {
        res.status(500).json(error)
    }
}

export const editStub = async (req:Request,res:Response) => {
    try{
        console.log(req.body);
        const stubRepository = getRepository(Stub);
        const id = req.params.id;
        let stubAModificar = await stubRepository.findOne(id);
        stubAModificar = stubRepository.merge(stubAModificar,req.body)
        const result = await stubRepository.save(stubAModificar);
        res.json(result)
    }catch (error) {
        console.log(error);
        res.status(500).json({ error });
    }
}

export const createStub = async (req: Request, res: Response) => {
    try {
        console.log(req.body);
        const stubRepository = getRepository(Stub);
        const lenguajeRepository = getRepository(Lenguaje);
        const stubNuevo = stubRepository.create(req.body as Stub)
        let lenguaje = await lenguajeRepository.findOne({ where: [{ lenguaje: stubNuevo.lenguaje }] })
        if(!lenguaje){
            lenguaje = lenguajeRepository.create(req.body as Lenguaje)
            lenguaje = await lenguajeRepository.save(lenguaje)
        }
        stubNuevo.lenguaje = lenguaje;
        
        const result = await stubRepository.save(stubNuevo)
        res.json(result)
    } catch (error) {
        console.log(error);
        res.status(500).json({ error });
    }
}


export const descargarStub = async (req: Request, res: Response) => {
    try {
        let descarga = req.query.archivo as string;
        let file = `${__dirname}/../../public/stubs/${descarga}`;
        return res.download(file, descarga, err => {
            console.log(err);
        })
    } catch (error) {
        res.status(500).json(error);
    }
}