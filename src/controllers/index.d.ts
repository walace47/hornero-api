// created from 'create-ts-index'

export * from './archivo.controller';
export * from './codigoUsuario.controller';
export * from './complejidad.controler';
export * from './estadoTorneo.controller';
export * from './etiqueta.controler';
export * from './jugar.controller';
export * from './lenguaje.controller';
export * from './login.cotroller';
export * from './problema.controller';
export * from './rol.controller';
export * from './stubs.controller';
export * from './tipoTorneo.controller';
export * from './torneos.controller';
export * from './usuario.controller';
