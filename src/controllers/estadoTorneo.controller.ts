import { Request, Response } from "express";
import { EstadoTorneo } from "../entity/EstadoTorneo";
import { getRepository } from "typeorm";

export const getEstados = async (req: Request, res: Response) => {
    try {
        let result = await getRepository(EstadoTorneo).find();
        res.json(result);
    } catch (error) {
        console.log(error);
        res.status(500).json({ error });
    }
};

export const getEstado = async (req: Request, res: Response) => {
    try {
        let id = req.params.id;
        if (Number(id)) {
            let result = await getRepository(EstadoTorneo).findOne(id);
            res.json(result)
        } else {
            res.status(400).json({ msg: "Id invalido" })
        }
    } catch (error) {
        res.status(500).json(error)
    }
}

export const createEstado = async (req: Request, res: Response) => {
    res.send({ msg: "Not implemented yet" })
}