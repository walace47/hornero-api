import { Request, Response } from "express";
import { Problema, problemaRelacion } from "../entity/Problema";
import { getRepository, getManager } from "typeorm";
import { Usuario } from "../entity/Usuario";
import { Etiqueta } from "../entity/Etiqueta";
import { Solucion } from "../entity/Solucion";

export const getProblemas = async (req: Request, res: Response) => {
	try {
		let select = JSON.parse(req.query.select as string)
		let relations =  JSON.parse(req.query.relations as string)
		let obj = null;
		obj = select ? {...obj, select} : obj; 
		obj = relations ? {...obj, relations} : obj; 
		let result = await getRepository(Problema).find({
			...obj
		});
		res.json(result);
	} catch (error) {
		console.log(error);
		res.status(500).json({ error });
	}
};

export const getProblema = async (req: Request, res: Response) => {
	try {
		let id = req.params.id;
		let select = req.query.select ? JSON.parse(req.query.select as string) : "";
		let relations = req.query.relations
			? JSON.parse(req.query.relations as string)
			: "";
		let result: Problema;
		if (Number(id)) {
			result = await getRepository(Problema).findOne({
				select: select,
				relations: relations,
				where: [{ idProblema: id }],
			});
			if (!result) {
				res.status(404).json({ msg: "problema no encontrado" });
			} else {
				res.json(result);
			}
		} else {
			res.status(400).json({ msg: "Id invalido" });
		}
	} catch (error) {
		console.log(error)
		res.status(500).json({ error });
	}
};

export const crearProblema = async (req: Request, res: Response) => {
	await getManager().transaction(async (transactionalEntityManager) => {
		try {
			const problemaRepository = transactionalEntityManager.getRepository(Problema);
			const nuevoProblema = problemaRepository.create(req.body as Problema);
			if (nuevoProblema.etiquetas) {
				const etiquetaRepository = transactionalEntityManager.getRepository(Etiqueta);
				for (let i = 0; i < nuevoProblema.etiquetas.length; i++) {
					let count = await etiquetaRepository.findAndCount({
						where: { etiqueta: nuevoProblema.etiquetas[i].etiqueta },
					});
					if (count[1] === 0) {
						await etiquetaRepository.save(nuevoProblema.etiquetas[i]);
					}
				}
			}
			let problemaInsertado = await problemaRepository.save(nuevoProblema);
			if (nuevoProblema.soluciones) {
				const solucionRepository = transactionalEntityManager.getRepository(Solucion);
				for (let i = 0; i < nuevoProblema.soluciones.length; i++) {
					let nuevaSolucion: Solucion = {
						...nuevoProblema.soluciones[i],
						problema: problemaInsertado,
					};
					await solucionRepository.save(nuevaSolucion);
				}
			}
			res.json(nuevoProblema)
		} catch (error) {
			res.status(500).json({ error, msg: "Error al crear un nuevo problema" });
		}
	});
};

export const editProblema = async (req: Request, res: Response) => {
	try {
		const nuevoDatosProblema: Problema = req.body;
		const id = req.params.id;
		const problemaRepository = getRepository(Problema);
		const solucionRepository = getRepository(Solucion);
		if (Number(id)) {
			const problemaAEditar = await getRepository(Problema).findOne(id, {
				relations: [problemaRelacion.SOLUCIONES],
			});
			if (problemaAEditar) {

				if (nuevoDatosProblema.etiquetas) {
					const etiquetaRepository = getRepository(Etiqueta);
					for (let i = 0; i < nuevoDatosProblema.etiquetas.length; i++) {
						const etiquetaActual = nuevoDatosProblema.etiquetas[i]
						let count = await etiquetaRepository
							.createQueryBuilder("e")
							.where("LOWER(e.etiqueta) like :etiqueta", { etiqueta: etiquetaActual.etiqueta })
							.getManyAndCount();

						if (count[1] === 0) {
							nuevoDatosProblema.etiquetas[i] = await etiquetaRepository.save(etiquetaActual);
						}

					}
				}
				console.log(nuevoDatosProblema, JSON.stringify(nuevoDatosProblema.etiquetas))

				const problemaMerge = problemaRepository.merge(problemaAEditar, nuevoDatosProblema);
				const result = await problemaRepository.save(problemaMerge);

				for (let i = 0; i < problemaMerge.soluciones.length; i++) {
					let problema = problemaRepository.create({
						idProblema: Number(id),
					} as Problema)
					problemaMerge.soluciones[i].problema = problema;
					await solucionRepository.save(problemaMerge.soluciones[i]);
				}


				res.json(result);
			} else {
				res.status(404).json({ msg: "problema no encontrado" });
			}
		} else {
			res.status(400).json({ msg: "Id invalido" });
		}
	} catch (error) {
		res.status(500).json({ error, msg: "Error al editar problema" });
	}
}
