import {MigrationInterface, QueryRunner} from "typeorm";

export class migracionCodigoUsuario1590253166954 implements MigrationInterface {
    name = 'migracionCodigoUsuario1590253166954'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `etiqueta` (`idEtiqueta` int NOT NULL AUTO_INCREMENT, `etiqueta` varchar(100) NOT NULL, PRIMARY KEY (`idEtiqueta`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `CodigoUsuario` (`idCodigoUsuario` int NOT NULL AUTO_INCREMENT, `codigo` text NOT NULL, `nombre` text NOT NULL, `idUsuario` int NOT NULL, PRIMARY KEY (`idCodigoUsuario`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("CREATE TABLE `problemaEtiqueta` (`idProblema` int NOT NULL, `idEtiqueta` int NOT NULL, INDEX `IDX_ca3fadebcb9b1bb6982d29b5fa` (`idProblema`), INDEX `IDX_2447aa4c1da5edaff18b78c07e` (`idEtiqueta`), PRIMARY KEY (`idProblema`, `idEtiqueta`)) ENGINE=InnoDB", undefined);
        await queryRunner.query("ALTER TABLE `problema` DROP FOREIGN KEY `FK_51da52f5de8f3b8a23df2822122`", undefined);
        await queryRunner.query("ALTER TABLE `problema` DROP COLUMN `idTipo`", undefined);
        await queryRunner.query("ALTER TABLE `CodigoUsuario` ADD CONSTRAINT `FK_ad7c679d9f9a6cc06281a576973` FOREIGN KEY (`idUsuario`) REFERENCES `usuario`(`idUsuario`) ON DELETE RESTRICT ON UPDATE RESTRICT", undefined);
        await queryRunner.query("ALTER TABLE `problemaEtiqueta` ADD CONSTRAINT `FK_ca3fadebcb9b1bb6982d29b5fa2` FOREIGN KEY (`idProblema`) REFERENCES `problema`(`idProblema`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("ALTER TABLE `problemaEtiqueta` ADD CONSTRAINT `FK_2447aa4c1da5edaff18b78c07e2` FOREIGN KEY (`idEtiqueta`) REFERENCES `etiqueta`(`idEtiqueta`) ON DELETE CASCADE ON UPDATE NO ACTION", undefined);
        await queryRunner.query("DROP TABLE `tipoproblema`", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `problemaEtiqueta` DROP FOREIGN KEY `FK_2447aa4c1da5edaff18b78c07e2`", undefined);
        await queryRunner.query("ALTER TABLE `problemaEtiqueta` DROP FOREIGN KEY `FK_ca3fadebcb9b1bb6982d29b5fa2`", undefined);
        await queryRunner.query("ALTER TABLE `CodigoUsuario` DROP FOREIGN KEY `FK_ad7c679d9f9a6cc06281a576973`", undefined);
        await queryRunner.query("ALTER TABLE `problema` ADD `idTipo` int NOT NULL", undefined);
        await queryRunner.query("DROP INDEX `IDX_2447aa4c1da5edaff18b78c07e` ON `problemaEtiqueta`", undefined);
        await queryRunner.query("DROP INDEX `IDX_ca3fadebcb9b1bb6982d29b5fa` ON `problemaEtiqueta`", undefined);
        await queryRunner.query("DROP TABLE `problemaEtiqueta`", undefined);
        await queryRunner.query("DROP TABLE `CodigoUsuario`", undefined);
        await queryRunner.query("DROP TABLE `etiqueta`", undefined);
    }

}
